#!/bin/sh -e

cd /home/thomas/univnautes-idp
. /home/thomas/venv/bin/activate
export UNIVNAUTES_IDP_SETTINGS_INI=/home/thomas/univnautes-idp/settings.ini.example 

TENANT=$(echo "$1" | tr -dc a-z0-9_-)
if [ x"$1" != x"$TENANT" ]
then
    echo $1 not a valid tenant name
    exit 1
fi

python << EOF
import os, sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "univnautes_idp.settings")
from entrouvert.djommon.multitenant.models import Tenant
if Tenant.objects.filter(schema_name="${TENANT}"):
    print >> sys.stderr, "tenant ${TENANT} already exists"
    sys.exit(1)
EOF

echo ""
echo "creating tenant..."
echo ""
python manage.py create-tenant $TENANT.univnautes-idp.dev.entrouvert.org $TENANT

echo ""
echo "creating 'admin' user in this tenant"
echo ""
python << EOF
import os, sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "univnautes_idp.settings")
from django.db import connection
from django.contrib.auth.models import User
connection.set_schema("${TENANT}")
u=User(username='admin')
u.set_password("${TENANT}")
u.is_staff=True
u.is_superuser=True
u.save()
print 'users:', User.objects.all()
EOF

echo ""
echo "tenant ${TENANT} created"
