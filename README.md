univnautes-idp : IdP multi-tenants pour UnivNautes

# config :
cp settings.ini.example /etc/univnautes-idp/settings.ini

# creation du schema public
python manage.py sync_schemas --shared --noinput
python manage.py migrate_schemas
python manage.py create-tenant univnautes-idp.dev.entrouvert.org public
python manage.py createsuperuser -s public

python manage.py create-tenant xyz.univnautes-idp.dev.entrouvert.org xyz
python manage.py createsuperuser -s xyz 


