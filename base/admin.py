from django.contrib import admin
from entrouvert.djommon.multitenant.models import Tenant, ClientSetting

class TenantAdmin(admin.ModelAdmin):
    list_display = ('schema_name', 'name', 'domain_url', 'is_active')
    
class ClientSettingAdmin(admin.ModelAdmin):
    list_display = ('tenant', 'name', 'value')
    
admin.site.register(Tenant, TenantAdmin)
admin.site.register(ClientSetting, ClientSettingAdmin)


