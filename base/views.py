import subprocess
from django.views.generic.edit import FormView
from django.contrib import messages
from django import forms
from entrouvert.djommon.multitenant.models import Tenant

CREATION_SCRIPT = '/home/thomas/univnautes-idp/create-dirty-tenant.py'

class TenantCreateForm(forms.Form):
    schema_name = forms.RegexField(regex='^[a-z0-9_-]+$',
         label='Nom',
         required=True,
         max_length=16)

class TenantCreateView(FormView):
    template_name = 'univnautes-idp/create-tenant.html'
    form_class = TenantCreateForm
    success_url = '/'

    def form_valid(self, form):
        schema_name = form.cleaned_data['schema_name']
        r = subprocess.call([CREATION_SCRIPT, schema_name])
        if r:
            messages.error(self.request, 'cannot create %s tenant (error %r)' % (schema_name, r))
        else:
            messages.info(self.request, '%s tenant created' % schema_name)
        return super(TenantCreateView, self).form_valid(form)

