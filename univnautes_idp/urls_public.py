from django.conf.urls import patterns, url, include
from django.views.generic import ListView, CreateView, RedirectView
from authentic2.urls import urlpatterns as authentic2_urlpatterns
from entrouvert.djommon.multitenant.models import Tenant
from base.views import TenantCreateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', ListView.as_view(
        model=Tenant,
        template_name='univnautes-idp/homepage.html')
    ),
    #url(r'^create/$', CreateView.as_view(model=Tenant)),
    url(r'^create/$', TenantCreateView.as_view()),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^logout/$', RedirectView.as_view(url='/admin/logout'), name='auth_logout'),
)
