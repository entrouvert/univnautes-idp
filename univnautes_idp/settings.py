# Django settings for univnautes_idp project.

import os
from ConfigParser import SafeConfigParser

# get configuration files from :
# 1. default-settings.ini from source code
# 2. os.environ.get('SETTINGS_INI') if it exists
#    else /etc/univnautes-idp/settings.ini
#         and then /etc/univnautes-idp/local-settings.ini

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SETTINGS_INI = (os.path.join(BASE_DIR, 'default-settings.ini'),)
if os.environ.get('SETTINGS_INI'):
    SETTINGS_INI += (os.environ.get('SETTINGS_INI'),)
else:
    ETC_DIR = os.path.join('/', 'etc', 'univnautes-idp')
    SETTINGS_INI += (
        os.path.join(ETC_DIR, 'settings.ini'),
        os.path.join(ETC_DIR, 'local-settings.ini')
    )

config = SafeConfigParser()
config.read(SETTINGS_INI)


DEBUG = config.getboolean('debug', 'general')
INTERNAL_IPS = tuple(config.get('debug', 'internal_ips').split())
TEMPLATE_DEBUG = config.getboolean('debug', 'template')
ADMINS = tuple(config.items('admins'))
MANAGERS = tuple(config.items('managers'))
SENTRY_DSN = config.get('debug', 'sentry_dsn')
DEBUG_TOOLBAR = config.getboolean('debug', 'toolbar')

DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': config.get('database','name'),
        'USER': config.get('database','user'),
        'PASSWORD': config.get('database','password'),
        'HOST': config.get('database','host'),
        'PORT': config.get('database','port'),
    }
}
SOUTH_DATABASE_ADAPTERS = {
    'default': 'south.db.postgresql_psycopg2',
}


# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
USE_X_FORWARDED_HOST = True

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'
gettext_noop = lambda s: s
LANGUAGES = (
    ('en', gettext_noop('English')),
    ('fr', gettext_noop('French')),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = config.get('dirs','media_root')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = config.get('dirs','static_root')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = tuple(config.get('dirs','static_dirs').split())

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = config.get('secrets', 'secret_key')

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'entrouvert.djommon.multitenant.template_loader.FilesystemLoader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.static',
    'authentic2.context_processors.federations_processor',
)

MIDDLEWARE_CLASSES = (
    'tenant_schemas.middleware.TenantMiddleware',
    'entrouvert.djommon.multitenant.middleware.EOTenantMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'authentic2.idp.middleware.DebugMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'univnautes_idp.wsgi.application'

TEMPLATE_DIRS = tuple(config.get('dirs', 'template_dirs').split())
MULTITENANT_TEMPLATE_DIRS = tuple(config.get('dirs', 'multitenant_template_dirs').split())

SHARED_APPS = (
    'base',
    'tenant_schemas',
    'entrouvert.djommon.multitenant',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'django.contrib.contenttypes',
    'south',
)

TENANT_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'django.contrib.contenttypes',
    'south',
    'registration',
    'authentic2.nonce',
    'authentic2.saml',
    'authentic2.idp',
    'authentic2.idp.saml',
    'authentic2.auth2_auth',
    'authentic2.attribute_aggregator',
    'authentic2.disco_service',
    'authentic2',
)

INSTALLED_APPS = SHARED_APPS + TENANT_APPS

# to override commands (hey, fixme if you can)
INSTALLED_APPS += ('tenant_schemas', 'entrouvert.djommon.multitenant',)

TENANT_MODEL = 'multitenant.Tenant'
PUBLIC_SCHEMA_NAME = 'public'

ROOT_URLCONF = 'univnautes_idp.urls'
PUBLIC_SCHEMA_URLCONF = 'univnautes_idp.urls_public'


SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

if config.getboolean('cache', 'memcached'):
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
        },
    }

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# email settings
EMAIL_HOST = config.get('email', 'host')
EMAIL_PORT = config.getint('email', 'port')
EMAIL_HOST_USER = config.get('email', 'user')
EMAIL_HOST_PASSWORD = config.get('email', 'password')
EMAIL_SUBJECT_PREFIX = config.get('email', 'subject_prefix')
EMAIL_USE_TLS = config.getboolean('email', 'use_tls')
SERVER_EMAIL = config.get('email', 'server_email')
DEFAULT_FROM_EMAIL = config.get('email', 'default_from_email')

# sessions
SESSION_EXPIRE_AT_BROWSER_CLOSE = config.get('session', 'expire_at_browser_close')

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login'
LOGOUT_URL = '/accounts/logout'

# Authentic2

DISCO_SERVICE = False
DISCO_USE_OF_METADATA = False
DISCO_SERVICE_NAME = 'http://www.identity-hub.com/disco_service/disco'
DISCO_RETURN_ID_PARAM = 'entityID'
SHOW_DISCO_IN_MD = False
USE_DISCO_SERVICE = False

# Authentication settings

AUTH_FRONTENDS = ('authentic2.auth2_auth.backend.LoginPasswordBackend',)
SSLAUTH_CREATE_USER = False
AUTHENTICATION_EVENT_EXPIRATION = 3600*24*7

# IdP settings

LOCAL_METADATA_CACHE_TIMEOUT = config.getint('saml', 'local_metadata_cache_timeout')
SAML_SIGNATURE_PUBLIC_KEY = config.get('saml', 'signature_public_key')
SAML_SIGNATURE_PRIVATE_KEY = config.get('saml', 'signature_private_key')
SAML_METADATA_AUTOLOAD = config.get('saml', 'metadata_autoload')

A2_CAN_RESET_PASSWORD = True
A2_REGISTRATION_CAN_DELETE_ACCOUNT = True
A2_REGISTRATION_EMAIL_IS_UNIQUE = True
REGISTRATION_OPEN = True
ACCOUNT_ACTIVATION_DAYS = 3
PASSWORD_RESET_TIMEOUT_DAYS = 3

# Admin tools
ADMIN_TOOLS_INDEX_DASHBOARD = 'univnautes_idp.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'univnautes_idp.dashboard.CustomAppIndexDashboard'
#ADMIN_TOOLS_MENU = 'authentic2.menu.CustomMenu'

# AUTH systels
AUTH_SAML2 = False
AUTH_OPENID = False
AUTH_SSL = False

# IdP protocols
IDP_SAML2 = True
IDP_OPENID = False
IDP_CAS = False

# List of IdP backends, mainly used to show available services in the homepage
# of user, and to handle SLO for each protocols
IDP_BACKENDS = ('authentic2.idp.saml.backend.SamlBackend',)


# debug toolbar needs more
if DEBUG_TOOLBAR:
    DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False}
    INSTALLED_APPS += ('debug_toolbar',)
    MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

# sentry needs more
if SENTRY_DSN:
    RAVEN_CONFIG = {'dsn': SENTRY_DSN}
    INSTALLED_APPS += ('raven.contrib.django.raven_compat',)

